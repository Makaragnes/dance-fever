package com.Dance.Fever.Moves.Knowledge.app;

import static com.Dance.Fever.Moves.Knowledge.app.FoviernaBNboanvaorn.asdbvaofvbasnbiuabrura;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    TextView totalQuestionsTextView;
    TextView aludvpuaibvpuiahg;
    Button erlkgnalkng, afdfsgsdg, dfbgkfgs;
    Button submitBtn;

    ImageView jakdsbaba, fdahhtjysfg;

    int dtrjyjdrtj=0;
    int totalQuestion = FoviernaBNboanvaorn.qsdfebabarstion.length;
    int reaharejdrhhr = 0;
    String afdbarghdshkytj = "";

    MediaPlayer mediaP = null;

    Boolean play = false;

    //MediaPlayer mp = null;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mediaP = MediaPlayer.create(this, R.raw.echo);
        play = (Boolean)getIntent().getBooleanExtra("mp", play);

        if (play) {
            mediaP.start();
        }

        aludvpuaibvpuiahg = findViewById(R.id.question);
        erlkgnalkng = findViewById(R.id.ans_A);
        afdfsgsdg = findViewById(R.id.ans_B);
        dfbgkfgs = findViewById(R.id.ans_C);

        fdahhtjysfg = findViewById(R.id.fdahhtjysfg);
        submitBtn = findViewById(R.id.submit_btn);

        erlkgnalkng.setOnClickListener(this);
        afdfsgsdg.setOnClickListener(this);
        dfbgkfgs.setOnClickListener(this);

        submitBtn.setOnClickListener(this);

        jakdsbaba = findViewById(R.id.jakdsbaba);

        jakdsbaba.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final Intent agrgherhrvvsr = new Intent(MainActivity.this, SataasActuvnoiayvity.class);
                agrgherhrvvsr.putExtra("mp", play);
                MainActivity.this.startActivity(agrgherhrvvsr);
                MainActivity.this.finish();
            }
        });

        strgmjtbtvtrybtrb();
    }

    @Override
    public void onClick(View view) {

        erlkgnalkng.setBackgroundColor(Color.WHITE);
        afdfsgsdg.setBackgroundColor(Color.WHITE);
        dfbgkfgs.setBackgroundColor(Color.WHITE);

        Button clickedButton = (Button) view;
        if(clickedButton.getId()==R.id.submit_btn){
            if(afdbarghdshkytj.equals(FoviernaBNboanvaorn.cdbytoarryrnyeycytAumnswweywars[reaharejdrhhr])){
                dtrjyjdrtj++;
            }
            reaharejdrhhr++;
            strgmjtbtvtrybtrb();


        }else{
            //choices button clicked
            afdbarghdshkytj  = clickedButton.getText().toString();
            if (afdbarghdshkytj.equals(FoviernaBNboanvaorn.cdbytoarryrnyeycytAumnswweywars[reaharejdrhhr])){
                clickedButton.setBackgroundColor(Color.GREEN);
            } else {
                clickedButton.setBackgroundColor(Color.RED);
            }

        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        mediaP.stop();
        //mp.pause();
    }

    void strgmjtbtvtrybtrb(){

        if(reaharejdrhhr == totalQuestion ){
            trsbrturtdtunyd();
            return;
        }

        int imageResource = getResources().getIdentifier(asdbvaofvbasnbiuabrura[reaharejdrhhr], null, getPackageName());

        Drawable res = getResources().getDrawable(imageResource);

        fdahhtjysfg.setBackground(res);

        aludvpuaibvpuiahg.setText(FoviernaBNboanvaorn.qsdfebabarstion[reaharejdrhhr]);
        erlkgnalkng.setText(FoviernaBNboanvaorn.carharbaoihnaces[reaharejdrhhr][0]);
        afdfsgsdg.setText(FoviernaBNboanvaorn.carharbaoihnaces[reaharejdrhhr][1]);
        dfbgkfgs.setText(FoviernaBNboanvaorn.carharbaoihnaces[reaharejdrhhr][2]);

    }

    void trsbrturtdtunyd(){
        String passStatus = "";
        if(dtrjyjdrtj > totalQuestion*0.60){
            passStatus = "Passed";
        }else{
            passStatus = "Failed";
        }

        new AlertDialog.Builder(this)
                .setTitle(passStatus)
                .setMessage("Score is "+ dtrjyjdrtj+" out of "+ totalQuestion)
                .setPositiveButton("Restart",(dialogInterface, i) -> restartQuiz() )
                .setCancelable(false)
                .show();


    }


    void restartQuiz(){
        dtrjyjdrtj = 0;
        reaharejdrhhr =0;
        strgmjtbtvtrybtrb();
    }
}