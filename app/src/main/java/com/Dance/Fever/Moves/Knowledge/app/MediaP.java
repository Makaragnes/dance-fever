package com.Dance.Fever.Moves.Knowledge.app;

import android.media.MediaPlayer;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class MediaP implements Parcelable {

    MediaPlayer mp = null;

    public MediaP(MediaPlayer mp){
        this.mp = mp;
    }

    protected MediaP(Parcel in) {
    }

    public static final Creator<MediaP> CREATOR = new Creator<MediaP>() {
        @Override
        public MediaP createFromParcel(Parcel in) {
            return new MediaP(in);
        }

        @Override
        public MediaP[] newArray(int size) {
            return new MediaP[size];
        }
    };

    void setMP(MediaPlayer mp){
        this.mp = mp;
    }
    MediaPlayer getMP(){
        return mp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
    }
}
