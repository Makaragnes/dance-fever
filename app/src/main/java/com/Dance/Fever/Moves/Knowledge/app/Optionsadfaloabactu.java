package com.Dance.Fever.Moves.Knowledge.app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class Optionsadfaloabactu extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener{

    ImageView jakdsbaba;

    SwitchCompat switchcompatds;
    Boolean play = true;

    MediaPlayer mediaP = null;


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_optionsadfaloabactu);

        switchcompatds = findViewById(R.id.switchcompatds);
        switchcompatds.setChecked(play);


        mediaP = MediaPlayer.create(this, R.raw.echo);

        jakdsbaba = findViewById(R.id.jakdsbaba);

        //MediaP mediaPOpti = (MediaP) getIntent().getSerializableExtra("mp");
        play = (Boolean)getIntent().getBooleanExtra("mp", play);

        switchcompatds.setOnCheckedChangeListener(this);

        if (play) {
            mediaP.start();
        }

        jakdsbaba.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final Intent ajksbguirdsd = new Intent(Optionsadfaloabactu.this, SataasActuvnoiayvity.class);
                ajksbguirdsd.putExtra("mp", play);
                Optionsadfaloabactu.this.startActivity(ajksbguirdsd);
                Optionsadfaloabactu.this.finish();
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//        Toast.makeText(this, "Отслеживание переключения: " + (isChecked ? "on" : "off"),
//                Toast.LENGTH_SHORT).show();
        play = isChecked;
        if (play) {
            mediaP.start();
        }
        if (!play) {
            mediaP.stop();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mediaP.stop();
    }

    @Override
    public void onBackPressed() {
        // your code.
        super.onBackPressed();
        final Intent hciruev = new Intent(Optionsadfaloabactu.this, SataasActuvnoiayvity.class);
        hciruev.putExtra("mp", play);
        Optionsadfaloabactu.this.startActivity(hciruev);
        Optionsadfaloabactu.this.finish();
    }
}