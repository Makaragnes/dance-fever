package kt.ob

import android.annotation.SuppressLint
import android.content.Context
import android.media.MediaPlayer
import com.Dance.Fever.Moves.Knowledge.app.R

@SuppressLint("StaticFieldLeak")
object Media {

    lateinit var context: Context;
    var mp: MediaPlayer? =null

    fun setMContext(context: Context){
        Media.context = context
    }

    fun initMedia(){
        mp = MediaPlayer.create(context, R.raw.echo)
    }

    fun play(){
        mp!!.start()
    }

    fun stop(){
        mp!!.stop()
    }

}